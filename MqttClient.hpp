#ifndef __MQTT_CLIENT_HPP
#define __MQTT_CLIENT_HPP

#include "Thermostat.hpp"

class MqttClient {
private:
    Thermostat& thermostat;
public:
    /**
     * @brief Construct an MQTT client to subscribe to room temperature change events.
     *
     * @param t Thermostat to be informed of the room temperature.
     */
    MqttClient(Thermostat& t) : thermostat(t) {}

    /**
     * @brief The function operator will start the MQTT client.
     */
    void operator()();
};

#endif // __MQTT_CLIENT_HPP