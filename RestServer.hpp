#ifndef __REST_SERVER_HPP
#define __REST_SERVER_HPP

#include <string>
#include "Thermostat.hpp"

class RestServer {
private:
    Thermostat& thermostat;
public:
    /**
     * @brief Construct a REST server to listen for thermostat user configuration.
     *
     * @param t Thermostat to be configured by the received requests.
     */
    RestServer(Thermostat& t) : thermostat(t) {}

    /**
     * @brief The function operator will start the REST server.
     */
    void operator()();
private:
    void rtrim(std::string&);
    std::string parse_request(std::istringstream);
    void handle_request(int client_socket);
};

#endif // __REST_SERVER_HPP