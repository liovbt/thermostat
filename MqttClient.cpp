#include "MqttClient.hpp"

#include <chrono>
#include <mqtt/client.h>

const std::string SERVER_ADDRESS { "mqtt://localhost:1883" };
const std::string CLIENT_ID { "thermostat" };

void MqttClient::operator()() {
    mqtt::client cli(SERVER_ADDRESS, CLIENT_ID);

    auto connOpts = mqtt::connect_options_builder()
        .clean_session(false)
        .finalize();

    const std::vector<std::string> TOPICS { "home/+/temperature" };
    const std::vector<int> QOS { 0 };

    try {
        std::cout << "Connecting to the MQTT server..." << std::flush;
        mqtt::connect_response rsp = cli.connect(connOpts);
        std::cout << "OK" << std::endl;

        if (!rsp.is_session_present()) {
            std::cout << "Subscribing to topics..." << std::flush;
            cli.subscribe(TOPICS, QOS);
            std::cout << "OK" << std::endl;
        }
        else {
            std::cout << "Session already present. Skipping subscribe." << std::endl;
        }

        while (true) {
            auto msg = cli.consume_message();

            if (msg) {
                if (msg->get_topic() == "home/room/temperature") {
                    int t;
                    try {
                        t = std::stoi(msg->to_string());
                    }
                    catch (std::invalid_argument const& ex)
                    {
                        std::cout << "Failed to parse room temperature: " << ex.what() << std::endl;
                        continue;
                    }
                    this->thermostat.setCurrentTemp(t);
                }
	        }
            else if (!cli.is_connected()) {
                std::cout << "Lost connection" << std::endl;
                while (!cli.is_connected()) {
                    std::this_thread::sleep_for(std::chrono::milliseconds(250));
                }
                std::cout << "Re-established connection" << std::endl;
            }
        }

        std::cout << "\nDisconnecting from the MQTT server..." << std::flush;
        cli.disconnect();
        std::cout << "OK" << std::endl;
    }
    catch (const mqtt::exception& exc) {
        std::cerr << exc.what() << std::endl;
        return;
    }
}