# Thermostat

## Content

This repo contains the implementation of a thermostat regulating the
temperature in a room. An MQTT client will subscribe to the room temperature.
An HTTP server with a REST interface allows to set the desired thresholds to
start heating and cooling. When the temperature is below the heating
threshold, the thermostat will activate the heater. When the temperature is
above the cooling threshold, the thermostat will activate the cooler.

Here is a class diagram.

```
┌────────────────────┐          ┌─────────────────────────────────┐
│       Heater       │          │            Thermostat           │
├────────────────────┤          ├─────────────────────────────────┤
│ - on : bool        │          │ - heatSetpoint : int            │
├────────────────────┤ 1        │ - coolSetpoint : int            │
│ + enable() : void  ├────────<>┤ - currentTemp : int             │
│ + disable() : void │          │ - heater : Heater               │
└────────────────────┘          │ - cooler : Cooler               │
                                ├─────────────────────────────────┤
┌────────────────────┐          │ - updateHeaterAndCooler() : int │
│       Cooler       │          │ + setHeatSetpoint(int) : int    │
├────────────────────┤ 1        │ + setCoolSetpoint(int) : int    │
│ - on : bool        ├────────<>┤ + setCurrentTemp(int) : int     │
├────────────────────┤          └────────────────────────┬────┬───┘
│ + enable() : void  │                                  1│   1│    
│ + disable() : void │   ┌──────────────────────────┐    │    │    
└────────────────────┘   │         MqttClient       │    │    │    
                         ├──────────────────────────┤    │    │    
                         │- thermostat : Thermostat ├<>──┘    │    
                         └──────────────────────────┘         │    
                                                              │    
┌───────────────────────────────────────────────────┐         │    
│                     RestServer                    │         │    
├───────────────────────────────────────────────────┤         │    
│ - thermostat : Thermostat                         ├<>───────┘    
├───────────────────────────────────────────────────┤              
│ - rtrim(std::string) : void                       │              
│ - parse_request(std::istringstream) : std::string │              
│ - handle_request(int client_socket) : void        │              
└───────────────────────────────────────────────────┘              
```

## Compilation

Required libraries are nlohmann_json and PahoMqttCpp.

```
cmake -B build .
cmake -B build -DCMAKE_PREFIX_PATH=$HOME/install . # in case libraries installed in $HOME/install
cmake --build build
```

## Usage

An MQTT broker needs to run at mqtt://localhost:1883.

```
# Run the thermostat
build/thermostat &

# Set the heating and cooling thresholds via REST
curl -X PUT http://localhost:8080/msp -d '{"heatSetpoint": 18, "coolSetpoint": 27}'

# Report the current room temperature via MQTT
python -c 'import paho.mqtt.publish as publish; publish.single("home/room/temperature", "35")'
```

## Unit tests

```
ctest --test-dir build/test
```

## Recording

[![asciicast](https://asciinema.org/a/644146.svg)](https://asciinema.org/a/644146)