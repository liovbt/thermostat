#include "Thermostat.hpp"
#include <iostream>

int Thermostat::setHeatSetpoint(int t) {
    std::cout << "Setting heatSetpoint to " << t << std::endl;
    this->heatSetpoint = t;
    return this->updateHeaterAndCooler();
}

int Thermostat::setCoolSetpoint(int t) {
    std::cout << "Setting coolSetpoint to " << t << std::endl;
    this->coolSetpoint = t;
    return this->updateHeaterAndCooler();
}

int Thermostat::setCurrentTemp(int t) {
    std::cout << "Setting current temperature to " << t << std::endl;
    this->currentTemp = t;
    return this->updateHeaterAndCooler();
}

int Thermostat::updateHeaterAndCooler() {
    if (this->currentTemp < this->heatSetpoint) {
        this->heater.enable();
    } else {
        this->heater.disable();
    }
    if (this->currentTemp > this->coolSetpoint) {
        this->cooler.enable();
    } else {
        this->cooler.disable();
    }
    return 0;
}