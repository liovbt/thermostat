#ifndef __HEATER_HPP
#define __HEATER_HPP

#include <iostream>

class Heater {
    bool on;
public:
    Heater() : on(false) {}

    /**
     * @brief Start heating
     */
    virtual void enable() { this->on = true; std::cout << "Heating started" << std::endl; }

    /**
     * @brief Stop heating
     */
    virtual void disable() { this->on = false; std::cout << "Heating stopped" << std::endl; }
};

#endif // __HEATER_HPP