#include <thread>
#include "RestServer.hpp"
#include "MqttClient.hpp"
#include "Thermostat.hpp"
#include "Heater.hpp"
#include "Cooler.hpp"

int main()
{
    Heater heater;
    Cooler cooler;
    Thermostat thermostat(heater, cooler);
    std::thread thread_rest{RestServer(thermostat)};
    std::thread thread_mqtt{MqttClient(thermostat)};
    thread_rest.join();
    thread_mqtt.join();
    return 0;
}