#include "RestServer.hpp"

#include <unistd.h>
#include <iostream>
#include <sstream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <nlohmann/json.hpp>

static const int PORT = 8080;

void RestServer::rtrim(std::string& s) {
    if (s.empty()) {
        return;
    }
    std::string::iterator p;
    for (p = s.end(); p != s.begin() && (std::isspace(*--p) || !std::isprint(*p)););
    if (!(std::isspace(*p) || !std::isprint(*p)))
        p++;
    s.erase(p, s.end());
}

std::string RestServer::parse_request(std::istringstream request) {
    std::string method, path, protocol;
    request >> method >> path >> protocol;

    //std::cout << "method=" << method << std::endl;
    //std::cout << "path=" << path << std::endl;
    //std::cout << "protocol=" << protocol << std::endl;

    if (method != "PUT") {
        return "HTTP/1.1 405 OK\r\nContent-Type: text/plain\r\n\r\n";
    }

    std::string line;
    bool foundBody = false;
    std::getline(request, line); // finish reading first line
    while (std::getline(request, line)) {
        rtrim(line);
        if (line.empty()) {
            foundBody = true;
            break;
        }
    }

    std::string response;
    if (!foundBody) {
        return "HTTP/1.1 400 OK\r\nContent-Type: text/plain\r\n\r\nMissing body in request\r\n";
    }

    std::getline(request, line);
    rtrim(line);

    nlohmann::json j;
    try {
        j = nlohmann::json::parse(line);
    }
    catch (nlohmann::json::parse_error& ex)
    {
        std::cout << "Failed to parse json, returning error 400" << std::endl;
        std::cout << ex.what() << std::endl;
        return "HTTP/1.1 400 OK\r\nContent-Type: text/plain\r\n\r\nInvalid json body\r\n";
    }

    if (j.contains("heatSetpoint")) {
        try {
            int t = j["heatSetpoint"];
            this->thermostat.setHeatSetpoint(t);
        }
        catch (const nlohmann::json::type_error& e) {
            std::cout << "Failed to parse heatSetpoint: " << e.what() << std::endl;
        }
    }

    if (j.contains("coolSetpoint")) {
        try {
            int t = j["coolSetpoint"];
            this->thermostat.setCoolSetpoint(t);
        }
        catch (const nlohmann::json::type_error& e) {
            std::cout << "Failed to parse coolSetpoint: " << e.what() << std::endl;
        }
    }
    
    return "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\nOK\r\n";
}

void RestServer::handle_request(int client_socket) {
    char buffer[1024] = {0};
    read(client_socket, buffer, sizeof(buffer));
    std::string response = parse_request(std::istringstream(buffer));
    write(client_socket, response.c_str(), response.size());
    close(client_socket);
}

void RestServer::operator()() {
    int server_socket = socket(AF_INET, SOCK_STREAM, 0);
    const int enable = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1)
        perror("setsockopt(SO_REUSEADDR) failed");

    sockaddr_in server_address{};
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address.sin_port = htons(PORT);
    if (bind(server_socket, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
        perror("Failed to bind socket");
        return;
    }
    if (listen(server_socket, 5) == -1) {
        perror("Failed to listen on socket");
        return;
    }
    std::cout << "REST server listening on port " << PORT << "..." << std::endl;

    while (true) {
        sockaddr_in client_address{};
        socklen_t client_address_len = sizeof(client_address);
        int client_socket = accept(server_socket, (struct sockaddr*)&client_address, &client_address_len);
        if (client_socket == -1) {
            perror("Failed to accept connection");
            break;
        }
        handle_request(client_socket);
    }

    close(server_socket);
}