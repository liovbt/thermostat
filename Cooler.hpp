#ifndef __COOLER_HPP
#define __COOLER_HPP

class Cooler {
    bool on;
public:
    Cooler() : on(false) {}

    /**
     * @brief Start cooling
     */
    virtual void enable() { this->on = true; std::cout << "Cooling started" << std::endl; }

    /**
     * @brief Stop cooling
     */
    virtual void disable() { this->on = false; std::cout << "Cooling stopped" << std::endl; }
};

#endif // __COOLER_HPP