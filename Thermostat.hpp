#ifndef __THERMOSTAT_HPP
#define __THERMOSTAT_HPP

#include "Heater.hpp"
#include "Cooler.hpp"

class Thermostat {
private:
    int heatSetpoint;
    int coolSetpoint;
    int currentTemp;
    Heater& heater;
    Cooler& cooler;
    int updateHeaterAndCooler();
public:
    Thermostat(Heater& h, Cooler& c) : heatSetpoint(30), coolSetpoint(0), currentTemp(15), heater(h), cooler(c) {}

    /**
     * @brief Set the heating threshold. When temperature is under this threshold, heating will be started.
     *
     * @param temp Temperature threshold in degree Celsius.
     *
     * @return 0 when successful
     */
    int setHeatSetpoint(int temperature);

    /**
     * @brief Set the cooling threshold. When temperature is above this threshold, cooling will be started.
     *
     * @param temp Temperature threshold in degree Celsius.
     *
     * @return 0 when successful
     */
    int setCoolSetpoint(int temperature);

    /**
     * @brief Set the current room temperature.
     *
     * @param temp Current room temperature in degree Celsius.
     *
     * @return 0 when successful
     */
    int setCurrentTemp(int temperature);
};

#endif // __THERMOSTAT_HPP