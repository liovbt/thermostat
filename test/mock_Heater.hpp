#include <gmock/gmock.h>

#include "../Heater.hpp"

class MockHeater : public Heater {
 public:
  MOCK_METHOD(void, enable, (), (override));
  MOCK_METHOD(void, disable, (), (override));
};
