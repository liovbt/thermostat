# Explicitly opt in to modern CMake behaviors to avoid warnings with recent 
# versions of CMake. 
cmake_policy(SET CMP0135 NEW)

include(FetchContent)
FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/03597a01ee50ed33e9dfd640b249b4be3799d395.zip
)
FetchContent_MakeAvailable(googletest)

enable_testing()

add_executable(
  test_Thermostat
  test_Thermostat.cpp
  ../Thermostat.cpp
)
target_link_libraries(
  test_Thermostat
  GTest::gtest_main
  GTest::gmock_main
)

include(GoogleTest)
gtest_discover_tests(test_Thermostat)