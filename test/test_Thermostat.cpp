#include <gtest/gtest.h>
#include "mock_Heater.hpp"
#include "mock_Cooler.hpp"
#include "../Thermostat.hpp"

TEST(Thermostat, VerifyHeaterEnabledWhenRoomTemperatureLow) {
    MockHeater heater;
    Cooler cooler;
    Thermostat thermostat(heater, cooler);
    thermostat.setHeatSetpoint(15);
    EXPECT_CALL(heater, enable()).Times(1);
    thermostat.setCurrentTemp(12);
}

TEST(Thermostat, VerifyHeaterDisabledWhenRoomTemperatureHigh) {
    MockHeater heater;
    Cooler cooler;
    Thermostat thermostat(heater, cooler);
    thermostat.setHeatSetpoint(10);
    EXPECT_CALL(heater, disable()).Times(1);
    thermostat.setCurrentTemp(31);
}

TEST(Thermostat, VerifyCoolerEnabledWhenRoomTemperatureHigh) {
    Heater heater;
    MockCooler cooler;
    Thermostat thermostat(heater, cooler);
    thermostat.setCoolSetpoint(30);
    EXPECT_CALL(cooler, enable()).Times(1);
    thermostat.setCurrentTemp(31);
}

TEST(Thermostat, VerifyCoolerDisabledWhenRoomTemperatureLow) {
    Heater heater;
    MockCooler cooler;
    Thermostat thermostat(heater, cooler);
    thermostat.setCoolSetpoint(30);
    EXPECT_CALL(cooler, disable()).Times(1);
    thermostat.setCurrentTemp(29);
}

TEST(Thermostat, VerifyHeaterEnabledWhenNewHighThreshold) {
    MockHeater heater;
    Cooler cooler;
    Thermostat thermostat(heater, cooler);
    thermostat.setCurrentTemp(20);
    EXPECT_CALL(heater, enable()).Times(1);
    thermostat.setHeatSetpoint(21);
}

TEST(Thermostat, VerifyHeaterDisabledWhenNewLowThreshold) {
    MockHeater heater;
    Cooler cooler;
    Thermostat thermostat(heater, cooler);
    thermostat.setCurrentTemp(20);
    EXPECT_CALL(heater, disable()).Times(1);
    thermostat.setHeatSetpoint(19);
}

TEST(Thermostat, VerifyCoolerEnabledWhenNewLowThreshold) {
    Heater heater;
    MockCooler cooler;
    Thermostat thermostat(heater, cooler);
    thermostat.setCurrentTemp(30);
    EXPECT_CALL(cooler, enable()).Times(1);
    thermostat.setCoolSetpoint(29);
}

TEST(Thermostat, VerifyCoolerDisabledWhenNewHighThreshold) {
    Heater heater;
    MockCooler cooler;
    Thermostat thermostat(heater, cooler);
    thermostat.setCurrentTemp(30);
    EXPECT_CALL(cooler, disable()).Times(1);
    thermostat.setCoolSetpoint(31);
}