#include <gmock/gmock.h>

#include "../Cooler.hpp"

class MockCooler : public Cooler {
 public:
  MOCK_METHOD(void, enable, (), (override));
  MOCK_METHOD(void, disable, (), (override));
};
